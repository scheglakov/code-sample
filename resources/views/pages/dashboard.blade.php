@extends('layouts.main')

@section('content')
    <h1 class="mb-2 mt-5">Statistics from {{Str::studly($currentRepository)}}.</h1>

    <div class="mb-5">
        @foreach(array_diff($repositoriesList, [$currentRepository]) as $repository)
            <a href="?repository={{$repository}}">
                Load from {{Str::studly($repository)}}
            </a>
            @if (!$loop->last)
                |
            @endif
        @endforeach
    </div>

    @if (!empty($error))
        <div class="alert alert-danger" role="alert">
            {{$error}}
        </div>
    @endif

    @if (!empty($statistics))
        <h4>Country statistics</h4>
        <table class="table table-sm">
            <tbody>
            <tr>
                <td>Average tax rate</td>
                <td>{{NumberFormat::formatTaxRate($statistics['countryAverageRate'], 3)}}</td>
            </tr>
            <tr>
                <td>Overall tax amount</td>
                <td>{{NumberFormat::formatTaxAmount($statistics['countryOverallAmount'])}}</td>
            </tr>
            </tbody>
        </table>

        <h4 class="mt-5">States statistics</h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">State</th>
                <th scope="col" class="text-right">Overall amount</th>
                <th scope="col" class="text-right">Average amount</th>
                <th scope="col" class="text-right">Average rate</th>
            </tr>
            </thead>
            <tbody>
            @foreach($states as $state)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$state}}</td>
                    <td class="text-right">{{NumberFormat::formatTaxAmount($statistics['statesOverallAmount'][$state] ?? 0)}}</td>
                    <td class="text-right">{{NumberFormat::formatTaxAmount($statistics['statesAverageAmount'][$state] ?? 0)}}</td>
                    <td class="text-right">{{NumberFormat::formatTaxRate($statistics['statesAverageRate'][$state] ?? 0, 3)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@stop

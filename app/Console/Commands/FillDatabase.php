<?php

namespace App\Console\Commands;

use App\DataProviders\ExcelTaxesProvider;
use App\Models\County;
use App\Models\CountyTax;
use App\Models\State;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FillDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taxes:fill-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill database from Excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // check if all tables were created
            if (!Schema::hasTable((new State)->getTable())
                || !Schema::hasTable((new County())->getTable())
                || !Schema::hasTable((new CountyTax())->getTable())
            ) {
                throw new \Exception('Please, check if migrations where deployed');
            }

            DB::beginTransaction();

            $excelDataProvider = new ExcelTaxesProvider(public_path('StatesTaxes.xlsx'));
            $statesData = $excelDataProvider->getStatesData();

            foreach ($statesData as $stateName => $counties) {
                // create state
                $state = State::create([
                    'name' => $stateName
                ]);

                if (empty($state->id)) {
                    throw new \Exception("Can not create state $stateName");
                }

                // add counties to state
                foreach ($counties as $county) {
                    $countyModel = County::create([
                        'state_id' => $state->id,
                        'name' => $county['county']
                    ]);

                    if (empty($countyModel->id)) {
                        throw new \Exception("Can not create county {$county['county']} in state $stateName");
                    }

                    // add taxes info for county
                    $countyTax = CountyTax::create([
                        'county_id' => $countyModel->id,
                        'tax_rate' => $county['tax_rate'],
                        'tax_amount' => $county['tax_amount'],
                    ]);

                    if (empty($countyTax->id)) {
                        throw new \Exception("Can not create county tax for county {$county['county']} in state $stateName");
                    }
                }
            }

//            throw new \Exception('Test');

            DB::commit();

            $this->info('Database was successfully filled');
        } catch (\Exception $e) {
            DB::rollBack();

            $this->error($e->getMessage());
        }
    }
}

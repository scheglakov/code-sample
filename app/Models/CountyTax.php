<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountyTax extends Model
{
    protected $fillable = ['county_id', 'tax_rate', 'tax_amount'];
}

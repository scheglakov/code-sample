<?php

namespace App\Http\Controllers;

use App\Statistics\StatisticsRepositoryFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
            $repository = $request->get('repository', 'database');

            $statistics = StatisticsRepositoryFactory::getStatisticsRepository($repository);

            View::share('currentRepository', $repository);
            View::share('repositoriesList', StatisticsRepositoryFactory::repositories);
            View::share('states', $statistics->states());
            View::share('statistics', [
                'statesOverallAmount' => $statistics->statesOverallAmount(),
                'statesAverageAmount' => $statistics->statesAverageAmount(),
                'statesAverageRate' => $statistics->statesAverageRate(),
                'countryOverallAmount' => $statistics->countryOverallAmount(),
                'countryAverageRate' => $statistics->countryAverageRate(),
            ]);

            return view('pages.dashboard');
        } catch (\Exception $e) {
            return view('pages.dashboard', [
                'error' => $e->getMessage()
            ]);
        }
    }
}

<?php

namespace App\Statistics;

interface StatisticsInterface
{
    /**
     * Return the overall amount of taxes collected per state
     * @return array
     */
    public function statesOverallAmount(): array;

    /**
     * Return the average amount of taxes collected per state
     * @return array
     */
    public function statesAverageAmount(): array;

    /**
     * Return the average county tax rate per state
     * @return array
     */
    public function statesAverageRate(): array;

    /**
     * Return the average tax rate of the country
     * @return float
     */
    public function countryAverageRate(): float;

    /**
     * Return the collected overall taxes of the country
     * @return float
     */
    public function countryOverallAmount(): float;

    /**
     * Return states
     * @return array
     */
    public function states(): array;
}

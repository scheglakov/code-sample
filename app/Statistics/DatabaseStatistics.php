<?php

namespace App\Statistics;

use App\Models\State;
use Illuminate\Support\Arr;

class DatabaseStatistics implements StatisticsInterface
{
    /**
     * @inheritDoc
     */
    public function statesOverallAmount(): array
    {
        $data = State::query()
            ->join('counties', 'states.id', '=', 'counties.state_id')
            ->join('county_taxes', 'counties.id', '=', 'county_taxes.county_id')
            ->selectRaw('states.name, SUM(county_taxes.tax_amount) AS overall_amount')
            ->groupBy('states.name')
            ->get()
            ->keyBy('name')
            ->pluck('overall_amount', 'name');

        return $data->toArray();
    }

    /**
     * @inheritDoc
     */
    public function statesAverageAmount(): array
    {
        $data = State::query()
            ->join('counties', 'states.id', '=', 'counties.state_id')
            ->join('county_taxes', 'counties.id', '=', 'county_taxes.county_id')
            ->selectRaw('states.name, AVG(county_taxes.tax_amount) AS average_amount')
            ->groupBy('states.name')
            ->get()
            ->keyBy('name')
            ->pluck('average_amount', 'name');

        return $data->toArray();
    }

    /**
     * @inheritDoc
     */
    public function statesAverageRate(): array
    {
        $data = State::query()
            ->join('counties', 'states.id', '=', 'counties.state_id')
            ->join('county_taxes', 'counties.id', '=', 'county_taxes.county_id')
            ->selectRaw('states.name, AVG(county_taxes.tax_rate) AS average_tax_rate')
            ->groupBy('states.name')
            ->get()
            ->keyBy('name')
            ->pluck('average_tax_rate', 'name');

        return $data->toArray();
    }

    /**
     * @inheritDoc
     */
    public function countryAverageRate(): float
    {
        $data = State::query()
            ->join('counties', 'states.id', '=', 'counties.state_id')
            ->join('county_taxes', 'counties.id', '=', 'county_taxes.county_id')
            ->selectRaw('AVG(county_taxes.tax_rate) AS average_tax_rate')
            ->get()
            ->pluck('average_tax_rate');

        return Arr::first($data->toArray());
    }

    /**
     * @inheritDoc
     */
    public function countryOverallAmount(): float
    {
        $statesOverallAmount = $this->statesOverallAmount();
        return array_sum($statesOverallAmount);
    }

    /**
     * @inheritDoc
     */
    public function states(): array
    {
        return State::query()
            ->select('name')
            ->get()
            ->pluck('name')
            ->toArray();
    }
}

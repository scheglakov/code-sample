<?php

namespace App\Statistics;

use App\DataProviders\ExcelTaxesProvider;

class ExcelStatistics implements StatisticsInterface
{
    private $excelDataProvider;

    public function __construct(string $excelFilePath)
    {
        $this->excelDataProvider = new ExcelTaxesProvider($excelFilePath);
    }

    /**
     * @inheritDoc
     */
    public function statesOverallAmount(): array
    {
        $statesOverallAmount = [];

        $statesData = $this->excelDataProvider->getStatesData();
        foreach ($statesData as $stateName => $counties) {
            $statesOverallAmount[$stateName] = array_sum(data_get($counties, '*.tax_amount'));
        }

        return $statesOverallAmount;
    }

    /**
     * @inheritDoc
     */
    public function statesAverageAmount(): array
    {
        $statesAverageAmount = [];

        $statesData = $this->excelDataProvider->getStatesData();
        foreach ($statesData as $stateName => $counties) {
            $taxAmounts = data_get($counties, '*.tax_amount');
            $statesAverageAmount[$stateName] = array_sum($taxAmounts) / sizeof($taxAmounts);
        }

        return $statesAverageAmount;
    }

    /**
     * @inheritDoc
     */
    public function statesAverageRate(): array
    {
        $statesAverageRate = [];

        $statesData = $this->excelDataProvider->getStatesData();
        foreach ($statesData as $stateName => $counties) {
            $taxRates = data_get($counties, '*.tax_rate');
            $statesAverageRate[$stateName] = array_sum($taxRates) / sizeof($taxRates);
        }

        return $statesAverageRate;
    }

    /**
     * @inheritDoc
     */
    public function countryAverageRate(): float
    {
        $statesData = $this->excelDataProvider->getStatesData();

        $taxRates = data_get($statesData, '*.*.tax_rate');

        if (sizeof($taxRates) === 0) {
            return 0;
        }

        return array_sum($taxRates) / sizeof($taxRates);
    }

    /**
     * @inheritDoc
     */
    public function countryOverallAmount(): float
    {
        $statesOverallAmount = $this->statesOverallAmount();
        return array_sum($statesOverallAmount);
    }

    /**
     * @inheritDoc
     */
    public function states(): array
    {
        $statesData = $this->excelDataProvider->getStatesData();
        return array_keys($statesData);
    }
}

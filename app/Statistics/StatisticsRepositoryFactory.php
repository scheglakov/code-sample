<?php

namespace App\Statistics;

class StatisticsRepositoryFactory
{
    /**
     * Possible repository types
     */
    public const repositories = [
        'excel',
        'database',
    ];

    /**
     * Get statistics repository
     * @param string $repository
     * @return StatisticsInterface
     * @throws \Exception
     */
    public static function getStatisticsRepository(string $repository): StatisticsInterface
    {
        switch ($repository) {
            case 'database':
                return new DatabaseStatistics();
                break;
            case 'excel':
                return new ExcelStatistics(public_path('StatesTaxes.xlsx'));
                break;
            default:
                throw new \Exception("Unknown Statistics Repository");
        }
    }
}
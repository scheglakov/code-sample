<?php

namespace App\DataProviders;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelTaxesProvider
{
    private $excelFilePath;
    private $states = [];

    public function __construct(string $excelFilePath)
    {
        $this->excelFilePath = $excelFilePath;
    }

    /**
     * Get states data from Excel file
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getStatesData()
    {
        if (!empty($this->states)) {
            return $this->states;
        }

        $reader = IOFactory::createReader("Xlsx");
        $spreadSheet = $reader->load($this->excelFilePath);
        $workSheet = $spreadSheet->getActiveSheet();

        $this->states = [];

        // iterate through all rows
        foreach ($workSheet->getRowIterator() as $row) {
            if ($row->getRowIndex() == 1) {
                continue;
            }

            $countyInfo = $this->getCountyInfo($workSheet, $row->getRowIndex());
            $this->states[$countyInfo['state']][] = [
                'county' => $countyInfo['county'],
                'tax_rate' => $countyInfo['tax_rate'],
                'tax_amount' => $countyInfo['tax_amount'],
            ];
        }

        return $this->states;
    }

    /**
     * Get collected county info from Excel row
     * @param $workSheet
     * @param $rowIndex
     * @return array
     */
    private function getCountyInfo(Worksheet $workSheet, int $rowIndex): array
    {
        $return = [];
        foreach (['state', 'county', 'tax_rate', 'tax_amount'] as $cellIndex => $key) {
            $return[$key] = $workSheet->getCellByColumnAndRow($cellIndex + 1, $rowIndex)->getCalculatedValue();
        }

        return $return;
    }
}

<?php

namespace App\Helpers;

class NumberFormat
{
    public static function formatTaxAmount(float $amount, $precision = 2)
    {
        return number_format($amount, $precision, '.', ' ');
    }

    public static function formatTaxRate(float $rate, $precision = 2)
    {
        return number_format($rate, $precision, '.', ' ');
    }
}

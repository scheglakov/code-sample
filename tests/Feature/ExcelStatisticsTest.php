<?php

namespace Tests\Feature;

use App\Statistics\ExcelStatistics;
use Tests\TestCase;

class ExcelStatisticsTest extends TestCase
{
    use UsesCommonTestMethods;

    public function setUp(): void
    {
        parent::setUp();

        $this->statisticsClass = new ExcelStatistics(public_path('StatesTaxes.xlsx'));
    }
}

<?php

namespace Tests\Feature;

trait UsesCommonTestMethods
{
    private $statisticsClass;

    public function testStatesOverallAmount()
    {
        $this->assertIsArray($this->statisticsClass->statesOverallAmount());
    }

    public function testStatesAverageAmount()
    {
        $this->assertIsArray($this->statisticsClass->statesAverageAmount());
    }

    public function testStatesAverageRate()
    {
        $this->assertIsArray($this->statisticsClass->statesAverageRate());
    }

    public function testCountryOverallAmount()
    {
        $this->assertIsFloat($this->statisticsClass->countryOverallAmount());
    }

    public function testCountryOverallRate()
    {
        $this->assertIsFloat($this->statisticsClass->countryOverallAmount());
    }
}

<?php

namespace Tests\Feature;

use App\Models\County;
use App\Models\CountyTax;
use App\Models\State;
use App\Statistics\DatabaseStatistics;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class DatabaseStatisticsTest extends TestCase
{
    use UsesCommonTestMethods;

    public function setUp(): void
    {
        parent::setUp();

        $this->statisticsClass = new DatabaseStatistics();
    }

    /**
     * Check if tables are exist
     *
     * @return void
     */
    public function testTablesExist()
    {
        $this->assertTrue(Schema::hasTable((new State)->getTable()));
        $this->assertTrue(Schema::hasTable((new County)->getTable()));
        $this->assertTrue(Schema::hasTable((new CountyTax)->getTable()));
    }
}

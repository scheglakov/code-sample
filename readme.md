### Task text:

Develop a software that can be used to calculate statistics about the tax income of a country. The country is organized in 5 states and theses states are devided into counties.

Each county has a different tax rate and collects a different amount of taxes.

The software should have the following features:

- Output the overall amount of taxes collected per state
- Output the average amount of taxes collected per state
- Output the average county tax rate per state
- Output the average tax rate of the country 
- Output the collected overall taxes of the country

Please use the MVC pattern in your implementation and implement two different datasources of your choice.

### Choosed datasources: DB (MySQL || PostgreSQL) and Excel

DB Schema: https://jl.jkli.ru/DB_UML.png

Excel file: https://jl.jkli.ru/StatesTaxes.xlsx

Excel file has calculated values for taxes rates and amounts, so it changes every time.

### Deployment
#### 1. Install composer packages

`composer install`

#### 2. Set local environment

Copy .env.example file to .env

`cp .env.example .env`

##### Set local application key

`php artisan key:generate`

##### PostgreSQL or MySQL connection (edit .env file)

`DB_CONNECTION=pgsql` OR `DB_CONNECTION=mysql`

`DB_HOST=`

`DB_PORT=`

`DB_DATABASE=`

`DB_USERNAME=`

`DB_PASSWORD=`

#### 3. Apply migrations

`php artisan migrate`

#### 4. Fill database from Excel file

`php artisan taxes:fill-database`

#### 5. Set up nginx

Example config: https://jl.jkli.ru/nginx.config

### Testing

`php vendor/phpunit/phpunit/phpunit`

### It's running here: https://jl.jkli.ru
